package vn.product.rfid.item.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import vn.product.rfid.category.service.Category;
import vn.product.rfid.category.service.CategoryDaoImpl;
@Repository("itemDao")
public class ItemDao {
	@Autowired
	private EntityManager entityManager;
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDaoImpl.class);
	
	public List<Item> items(String keyword) {
		// TODO Auto-generated method stub
		String s = "Select id From Item a";

		Query query = entityManager.createQuery(s, Item.class);
		/*
		 * query.setParameter("status", status); query.setParameter("code", "%" +
		 * keyword + "%"); query.setParameter("description", "%" + keyword + "%");
		 */
		List<Category> l = query.getResultList();
		LOGGER.trace(l.toString());
		return query.getResultList();

		
	}
}
