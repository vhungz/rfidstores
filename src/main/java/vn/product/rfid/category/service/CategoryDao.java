package vn.product.rfid.category.service;

import java.util.List;

public interface CategoryDao {
	public void insert(Category m);
	public int update(Category m);
	public int delete(String id);
	public Category category(String id);
	public List<Category> list(String lang, String keyword);
	
}
