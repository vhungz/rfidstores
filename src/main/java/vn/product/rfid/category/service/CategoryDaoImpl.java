package vn.product.rfid.category.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("categoryDao")
public class CategoryDaoImpl implements CategoryDao {

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDaoImpl.class);
	@Override
	public void insert(Category m) {
		entityManager.persist(m);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> list(String lang, String keyword) {
		// TODO Auto-generated method stub

		String s = "Select a.id,"											
				+ "		   a.parentid,"
				+ "		   a.created_dt,"
				+ "		   a.last_updated_dt,"
				+ "		   case when :lang = \'en\' then a.ennm else a.vienm end as nm,"				
				+ "		   a.status"
				+ " From category a";
		
		//String s = "Select a from Category a";
				return  entityManager
						.createNativeQuery(s,Category.class)				
						.setParameter("lang", lang)
						.getResultList();
	}

	@Override
	public int update(Category m) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Category category(String id) {
		// TODO Auto-generated method stub
		return null;
	}

}
