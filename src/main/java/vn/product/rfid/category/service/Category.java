package vn.product.rfid.category.service;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="Category")
@Table(name="category")
public class Category   {

	@Id
	@Column(name="id")
	private String id;
	
	
	@Column(name="parentid")
	private String parentid;
	
	@Column(name="created_dt")
	private Date created_dt;
	
	@Column(name="last_updated_dt")
	private Date last_updated_dt;
	
	@Column(name="status")
	private int status;
	
	@Column(name="nm")
	private String nm;
	
	public Category() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public Date getCreated_dt() {
		return created_dt;
	}

	public void setCreated_dt(Date created_dt) {
		this.created_dt = created_dt;
	}

	public Date getLast_updated_dt() {
		return last_updated_dt;
	}

	public void setLast_updated_dt(Date last_updated_dt) {
		this.last_updated_dt = last_updated_dt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	public String getNm() {
		return nm;
	}

	public void setNm(String nm) {
		this.nm = nm;
	}

	@Override
	public String toString() {
		return "Category [id=" + id +  ", parentid=" + parentid + ", created_dt="
				+ created_dt + ", last_updated_dt=" + last_updated_dt + ", status=" + status + ", nm=" + nm + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((created_dt == null) ? 0 : created_dt.hashCode());
	
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((last_updated_dt == null) ? 0 : last_updated_dt.hashCode());
		result = prime * result + ((nm == null) ? 0 : nm.hashCode());
		result = prime * result + ((parentid == null) ? 0 : parentid.hashCode());
		result = prime * result + status;
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (created_dt == null) {
			if (other.created_dt != null)
				return false;
		} else if (!created_dt.equals(other.created_dt))
			return false;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (last_updated_dt == null) {
			if (other.last_updated_dt != null)
				return false;
		} else if (!last_updated_dt.equals(other.last_updated_dt))
			return false;
		if (nm == null) {
			if (other.nm != null)
				return false;
		} else if (!nm.equals(other.nm))
			return false;
		if (parentid == null) {
			if (other.parentid != null)
				return false;
		} else if (!parentid.equals(other.parentid))
			return false;
		if (status != other.status)
			return false;
	
		return true;
	}
	
	


}
