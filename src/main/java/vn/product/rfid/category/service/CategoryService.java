package vn.product.rfid.category.service;

import java.util.List;

public interface CategoryService {
	List<Category> list(String lang, String keyword);
}
