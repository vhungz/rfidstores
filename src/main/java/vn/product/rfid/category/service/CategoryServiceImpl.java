package vn.product.rfid.category.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("categoryService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class CategoryServiceImpl implements CategoryService{
	@Autowired
	private CategoryDao categoryDao;
	@Override
	public List<Category> list(String lang, String keyword) {
		
		return categoryDao.list(lang, keyword);
	}
	
}
