package vn.product.rfid.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.product.rfid.category.service.Category;
import vn.product.rfid.category.service.CategoryService;

@Controller
public class HomeController {
	@Autowired
	CategoryService ct;
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/")
	public String index() {
		return "public/index";
	}
}
