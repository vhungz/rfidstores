package vn.product.rfid.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {
	@RequestMapping("/dashboard")
	public String index() {
		return "/admin/dashboard";
	}
	
	@RequestMapping("/admin/login")
	public String login() {
		return "admin/login";
	}
}
