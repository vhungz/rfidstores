package vn.product.rfid.categorytype.service;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity(name="CategoryType")
@Table(name="category_type")
public class CategoryTypeModel {
	@Id
	private String id;
	@Column(name ="ennm")
	private String ennm;
	@Column(name ="vienm")
	private String vienm;
	
	@Column(name ="created_dt")
	private Date created_dt;
	@Column(name ="last_updated_dt")
	private Date last_updated_dt;
	@Column(name ="created_by")
	private String created_by;
	@Column(name ="updated_by")
	private String updated_by;
	@Column(name ="status")
	private int status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEnnm() {
		return ennm;
	}
	public void setEnnm(String ennm) {
		this.ennm = ennm;
	}
	public String getVienm() {
		return vienm;
	}
	public void setVienm(String vienm) {
		this.vienm = vienm;
	}
	public Date getCreated_dt() {
		return created_dt;
	}
	public void setCreated_dt(Date created_dt) {
		this.created_dt = created_dt;
	}
	public Date getLast_updated_dt() {
		return last_updated_dt;
	}
	public void setLast_updated_dt(Date last_updated_dt) {
		this.last_updated_dt = last_updated_dt;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((created_by == null) ? 0 : created_by.hashCode());
		result = prime * result + ((created_dt == null) ? 0 : created_dt.hashCode());
		result = prime * result + ((ennm == null) ? 0 : ennm.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((last_updated_dt == null) ? 0 : last_updated_dt.hashCode());
		result = prime * result + status;
		result = prime * result + ((updated_by == null) ? 0 : updated_by.hashCode());
		result = prime * result + ((vienm == null) ? 0 : vienm.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoryTypeModel other = (CategoryTypeModel) obj;
		if (created_by == null) {
			if (other.created_by != null)
				return false;
		} else if (!created_by.equals(other.created_by))
			return false;
		if (created_dt == null) {
			if (other.created_dt != null)
				return false;
		} else if (!created_dt.equals(other.created_dt))
			return false;
		if (ennm == null) {
			if (other.ennm != null)
				return false;
		} else if (!ennm.equals(other.ennm))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (last_updated_dt == null) {
			if (other.last_updated_dt != null)
				return false;
		} else if (!last_updated_dt.equals(other.last_updated_dt))
			return false;
		if (status != other.status)
			return false;
		if (updated_by == null) {
			if (other.updated_by != null)
				return false;
		} else if (!updated_by.equals(other.updated_by))
			return false;
		if (vienm == null) {
			if (other.vienm != null)
				return false;
		} else if (!vienm.equals(other.vienm))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CategoryTypeModel [id=" + id + ", ennm=" + ennm + ", vienm=" + vienm + ", created_dt=" + created_dt
				+ ", last_updated_dt=" + last_updated_dt + ", created_by=" + created_by + ", updated_by=" + updated_by
				+ ", status=" + status + "]";
	}
	
	
}
