package vn.product.rfid.categorytype.service;

import java.util.List;

public interface CategoryTypeDao {
	public List<CategoryTypeModel> list(int status, String keyword);
}
