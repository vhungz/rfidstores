package vn.product.rfid.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vn.product.rfid.category.service.Category;
import vn.product.rfid.category.service.CategoryService;

@RestController
public class CategoryApi {
	@Autowired
	private CategoryService categoryService;
	@RequestMapping(value = "api/v1/category/list", method = RequestMethod.GET)
	public ResponseEntity<List<Category>>  categoryList(@RequestParam(value = "lang", defaultValue = "en") String lang) {
		System.out.println(lang);
		return new ResponseEntity<List<Category>>(categoryService.list(lang, ""), HttpStatus.OK); 
	}
	
}
