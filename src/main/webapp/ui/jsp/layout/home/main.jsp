<!DOCTYPE html>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page isELIgnored="false"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="MediaCenter">
<meta name="robots" content="all">
<link rel="stylesheet"
	href="/ui/static/assets/font-awesome/4.5.0/css/font-awesome.min.css" />


<link rel="stylesheet" href="/ui/static/assets/css/main.css" />
<link rel="stylesheet" href="/ui/static/assets/css/blue.css" />
<link rel="stylesheet" href="/ui/static/assets/css/styles.css" />

<link rel="stylesheet" href="/ui/static/assets/css/mystyle.css" />
<link rel="stylesheet" href="/ui/static/assets/css/ui.all.css" />
<!-- <link rel="stylesheet" href="/ui/static/assets/css/white.css" /> -->
<link rel="stylesheet" href="/ui/static/assets/css/media.css" />

<link rel="stylesheet" href="/ui/static/assets/css/incipit.min.css" />
<!-- <link rel="stylesheet" href="/ui/static/assets/css/lightbox.css" /> -->

<!-- <link rel="stylesheet" href="/ui/static/assets/css/animate.min.css" /> -->
<link rel="stylesheet"
	href="/ui/static/assets/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="/ui/static/assets/css/config.css">
<!-- <link href="/ui/static/assets/css/css" rel="stylesheet" type="text/css"> -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="/ui/static/assets/js/bootstrap-hover-dropdown.min.js"></script>
<script src="/ui/static/assets/js/incipit/incipit.js"></script>
<script src="/ui/static/assets/js/bootstrap-select.min.js"></script>

<script src="/ui/static/assets/js/bootstrap-slider.min.js"></script>

<title><spring:message code="home.title"></spring:message></title>
</head>
<script>
			//loading indicator
		
		  // 'arrow', 'download', 'upload'
		
		  // 'solid-snake', 'penduleum'
		
		  // 'fading-balls', 'fading-lines'
		
		  // 'round-block', 'oval-circle'

	$(document).ready(function(){
		$('body').IncipitInit({			
			icon : "solid-snake",
			note : false,

			});

		$.Incipit('show');
	/* 	<ul class="nav nav-pills nav-stacked collapse in" id="p1">
		<li><a href="google.com">View One</a></li>
		<li><a href="gmail.com">View Two</a></li>
		<li class="nav-divider"></li>
		<li><a href="#">Trash</a></li><a data-toggle="collapse" data-parent="#stacked-menu" href="#p1" aria-expanded="false" class="collapsed">Process One<span class="caret arrow"></span></a>
	</ul> */
		$.get('api/v1/category/list',function(data, status){
			console.log(data)
			$.Incipit('hide');
			var html = "";
			for(var i= 0; i<data.length; i++){
				if (data[i].parentid === null){					
					html = '<li><a data-toggle="collapse" aria-expanded="false" class="collapsed" data-parent="#stacked-menu"	href="#p-'+i+10+'" >'+data[i].nm+'<span class="caret arrow"></span></a>';
					html += '<ul class="nav nav-pills nav-stacked collapse" id="p-' + i +10 +'" >'
					for(var j=0 ;j< data.length; j++){
						
						if (data[j].parentid === data[i].id){
							html += '<li><a href="#">'+ data[j].nm + '</a></li>';
						}
					}
					html += '<li class="nav-divider"></li></ul></li>';
					$('#stacked-menu').append(html);
					console.log(html)
				}
			}
		});
	});
	
	
</script>
<body class="cnt-home">

	<header class="header-style-1 header-style-2">

		<!-- ============================================== TOP MENU ============================================== -->
		<div class="top-bar animate-dropdown">
			<div class="container">
				<div class="header-top-inner">
					<div class="cnt-account">
						<!-- <ul class="list-unstyled">
							<li><a
								href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
									class="icon fa fa-user"></i>My Account</a></li>
							<li><a
								href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
									class="icon fa fa-heart"></i>Wishlist</a></li>
							<li><a
								href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
									class="icon fa fa-shopping-cart"></i>My Cart</a></li>
							<li><a
								href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
									class="icon fa fa-key"></i>Checkout</a></li>
							<li><a
								href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
									class="icon fa fa-sign-in"></i>Login</a></li>
						</ul> -->
					</div>
					<!-- /.cnt-account -->

					<div class="cnt-block">
						<ul class="list-unstyled list-inline">

							<li class="dropdown dropdown-small"><a
								href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"
								class="dropdown-toggle" data-hover="dropdown"
								data-toggle="dropdown"><span class="key">language :</span><span
									class="value">English </span><b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">English</a></li>
									<li><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Việt Nam</a></li>
								</ul></li>
						</ul>
						<!-- /.list-unstyled -->
					</div>
					<!-- /.cnt-cart -->
					<div class="clearfix"></div>
				</div>
				<!-- /.header-top-inner -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.header-top -->
		<!-- ============================================== TOP MENU : END ============================================== -->
		<div class="main-header">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
						<!-- ============================================================= LOGO ============================================================= -->
						<div class="logo">
							<a href="#"> <img src="/ui/static/assets/images/nfc.png"
								width="180" height="100" alt="">

							</a>
						</div>
						<!-- /.logo -->
						<!-- ============================================================= LOGO : END ============================================================= -->
					</div>
					<!-- /.logo-holder -->

					<div class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
						<div class="contact-row">
							<div class="phone inline">
								<i class="icon fa fa-phone"></i> (0084) 972 805 265
							</div>
							<div class="contact inline">
								<i class="icon fa fa-envelope"></i> viethungz@gmail.com
							</div>
						</div>
						<!-- /.contact-row -->
						<!-- ============================================================= SEARCH AREA ============================================================= -->
						<div class="search-area">
							<form>
								<div class="control-group" style="height: 46px;">

									<ul class="categories-filter animate-dropdown">
										<li class="dropdown"><a class="dropdown-toggle"
											data-toggle="dropdown" href="#"><spring:message
													code="home.label.categories"></spring:message> <b
												class="caret"></b> </a>
										<!-- search -->
										<!-- 	<ul class="dropdown-menu" role="menu">
												<li class="menu-header">Computer</li>
												<li role="presentation"><a role="menuitem"
													tabindex="-1"
													href="https://demo2.chethemes.com/html/unicase/index.php?page=category">-
														Laptops</a></li>
												<li role="presentation"><a role="menuitem"
													tabindex="-1"
													href="https://demo2.chethemes.com/html/unicase/index.php?page=category">-
														Tv &amp; audio</a></li>
												<li role="presentation"><a role="menuitem"
													tabindex="-1"
													href="https://demo2.chethemes.com/html/unicase/index.php?page=category">-
														Gadgets</a></li>
												<li role="presentation"><a role="menuitem"
													tabindex="-1"
													href="https://demo2.chethemes.com/html/unicase/index.php?page=category">-
														Cameras</a></li>

											</ul> -->
											</li>
									</ul>

									<input class="search-field" placeholder="Search here...">

									<a class="search-button"
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"></a>

								</div>
							</form>
						</div>
						<!-- /.search-area -->
						<!-- ============================================================= SEARCH AREA : END ============================================================= -->
					</div>
					<!-- /.top-search-holder -->


				</div>
				<!-- /.row -->

			</div>
			<!-- /.container -->

		</div>
		<!-- /.main-header -->

		<!-- ============================================== NAVBAR ============================================== -->
		<div class="header-nav animate-dropdown">
			<div class="container">
				<div class="yamm navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button data-target="#mc-horizontal-menu-collapse"
							data-toggle="collapse" class="navbar-toggle collapsed"
							type="button">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<div class="nav-bg-class">
						<div class="navbar-collapse collapse"
							id="mc-horizontal-menu-collapse">
							<div class="nav-outer">
								<ul class="nav navbar-nav">
									<li class="active dropdown yamm-fw"><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=home"
										data-hover="dropdown" class="dropdown-toggle"
										data-toggle="dropdown"> <spring:message code="home.nav.rfidtag"></spring:message></a>
										<ul class="dropdown-menu">
											<li>
												<div class="yamm-content">
													<div class="row">
														<div class="col-md-8 col-sm-8">
															<div class="row">
																<div class="col-md-12">

																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<h2 class="title">Computer</h2>
																		<ul class="links">
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Lenovo</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Microsoft
																			</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Fuhlen</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Longsleeves</a></li>
																		</ul>
																	</div>
																	<!-- /.col -->

																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<h2 class="title">Camera</h2>
																		<ul class="links">
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Fuhlen</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Lenovo</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Microsoft
																			</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Longsleeves</a></li>
																		</ul>
																	</div>
																	<!-- /.col -->

																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<h2 class="title">Apple Store</h2>
																		<ul class="links">
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Longsleeves</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Fuhlen</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Lenovo</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Microsoft
																			</a></li>
																		</ul>
																	</div>
																	<!-- /.col -->

																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<h2 class="title">Smart Phone</h2>
																		<ul class="links">
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Microsoft
																			</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Longsleeves</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Fuhlen</a></li>
																			<li><a
																				href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Lenovo</a></li>

																		</ul>
																	</div>
																	<!-- /.col -->

																</div>

															</div>
														</div>
														<div class="col-sm-4"></div>
													</div>
													<!-- /.row -->

													<!-- ============================================== WIDE PRODUCTS ============================================== -->
													<div class="wide-banners megamenu-banner">
														<div class="row">
															<div class="col-sm-12 col-md-8">
																<div class="row">
																	<div class="col-md-12">
																		<div class="col-sm-6 col-md-6">
																			<div class="wide-banner cnt-strip">
																				<div class="image">
																					<!-- <img class="img-responsive"
																						src="./Unicase_files/4.jpg" alt=""> -->
																				</div>
																				<div class="strip">
																					<div class="strip-inner text-right">
																						<h3 class="white">new trend</h3>
																						<h2 class="white">apple product</h2>
																					</div>
																				</div>
																			</div>
																			<!-- /.wide-banner -->
																		</div>
																		<!-- /.col -->

																		<div class="col-sm-6 col-md-6">
																			<div class="wide-banner cnt-strip">
																				<div class="image">
																					<!-- <img class="img-responsive"
																						src="./Unicase_files/5.jpg" alt=""> -->
																				</div>
																				<div class="strip">
																					<div class="strip-inner text-left">
																						<h3 class="white">camera collection</h3>
																						<h2 class="white">new arrivals</h2>
																					</div>
																				</div>
																			</div>
																			<!-- /.wide-banner -->
																		</div>
																		<!-- /.col -->
																	</div>

																</div>
																<!-- /.row -->
															</div>

														</div>
													</div>
													<!-- /.wide-banners -->

													<!-- ============================================== WIDE PRODUCTS : END ============================================== -->

												</div> <!-- /.yamm-content -->
											</li>
										</ul></li>

									<li class="dropdown"><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=category">Men

									</a></li>

									<li class="dropdown"><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=category">Women

									</a></li>
									<li class="dropdown hidden-sm"><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=category">New
											Arrivals <!-- <span class="menu-label new-menu hidden-xs">new</span> -->
									</a></li>

									<li class="dropdown"><a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"
										class="dropdown-toggle" data-hover="dropdown"
										data-toggle="dropdown">Pages</a>
										<ul class="dropdown-menu pages">
											<li>
												<div class="yamm-content">
													<div class="row">

														<div class="col-xs-12 col-sm-4 col-md-4">
															<ul class="links">
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=home">Home
																		I</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=home2">Home
																		II</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=category">Category</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=category-2">Category
																		II</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=detail">Detail</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=detail2">Detail
																		II</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=shopping-cart">Shopping
																		Cart Summary</a></li>

															</ul>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4">
															<ul class="links">
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=checkout">Checkout</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=blog">Blog</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=blog-details">Blog
																		Detail</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=contact">Contact</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage1">Homepage
																		1</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2">Homepage
																		2</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=home-furniture">Home
																		Furniture</a></li>
															</ul>
														</div>
														<div class="col-xs-12 col-sm-4 col-md-4">
															<ul class="links">
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=sign-in">Sign
																		In</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=my-wishlist">Wishlist</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=terms-conditions">Terms
																		and Condition</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=track-orders">Track
																		Orders</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=product-comparison">Product-Comparison</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=faq">FAQ</a></li>
																<li><a
																	href="https://demo2.chethemes.com/html/unicase/index.php?page=404">404</a></li>
															</ul>

														</div>

													</div>
												</div>
											</li>


										</ul></li>


								</ul>
								<!-- /.navbar-nav -->
								<div class="clearfix"></div>
							</div>
							<!-- /.nav-outer -->
						</div>
						<!-- /.navbar-collapse -->


					</div>
					<!-- /.nav-bg-class -->
				</div>
				<!-- /.navbar-default -->
			</div>
			<!-- /.container-class -->

		</div>
		<!-- /.header-nav -->
		<!-- ============================================== NAVBAR : END ============================================== -->

	</header>
	<div class="homepage-container" >
		<div class="container">

			<div class="col-md-2" style="margin-top:10px;border:1px solid;">
			
				<ul class="nav nav-pills nav-stacked" id="stacked-menu">
					<li class="category-type"> Products </li>
					<!-- <li><a data-toggle="collapse" data-parent="#stacked-menu"
						href="#p1">Process One<span class="caret arrow"></span></a>
						<ul class="nav nav-pills nav-stacked collapse in" id="p1">
							<li><a href="google.com">View One</a></li>
							<li><a href="gmail.com">View Two</a></li>
							<li class="nav-divider"></li>
							<li><a href="#">Trash</a></li>
						</ul></li>
					<li><a data-toggle="collapse" data-parent="#stacked-menu"
						href="#p2">Process Two<span class="caret arrow"></span></a>
						<ul class="nav nav-pills nav-stacked collapse" id="p2">
							<li><a href="#">View One</a></li>
							<li><a href="#">View Two</a></li>
							<li><a href="#">View Three</a></li>
							<li class="nav-divider"></li>
							<li><a href="#">Trash</a></li>
						</ul></li>
					<li><a href="#">Process Three<span class="caret arrow"></span></a></li>
					<li><a href="#">Process Four<span class="caret arrow"></span></a></li> -->
				</ul>

			</div>
			<div class="col-md-8">
				<tiles:insertAttribute name="content" />
			</div>
		</div>
	</div>

	<!-- ============================================================= FOOTER ============================================================= -->
	<footer id="footer" class="footer color-bg">
		<div class="links-social inner-top-sm">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<!-- ============================================================= CONTACT INFO ============================================================= -->
						<div class="contact-info">
							<div class="footer-logo">
								<div class="logo">
									<a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=home">

										<!-- <img src="./Unicase_files/logo.png" alt=""> -->

									</a>
								</div>
								<!-- /.logo -->

							</div>
							<!-- /.footer-logo -->

							<div class="module-body m-t-20">
								<p class="about-us">Nam libero tempore, cum soluta nobis est
									ses eligendi optio cumque cum soluta nobis est ses eligendi
									optio cumque.</p>

								<div class="social-icons">

									<a href="http://facebook.com/transvelo" class="active"><i
										class="icon fa fa-facebook"></i></a> <a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
										class="icon fa fa-twitter"></i></a> <a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
										class="icon fa fa-linkedin"></i></a> <a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
										class="icon fa fa-rss"></i></a> <a
										href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#"><i
										class="icon fa fa-pinterest"></i></a>

								</div>
								<!-- /.social-icons -->
							</div>
							<!-- /.module-body -->

						</div>
						<!-- /.contact-info -->
						<!-- ============================================================= CONTACT INFO : END ============================================================= -->
					</div>
					<!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-3">
						<!-- ============================================================= CONTACT TIMING============================================================= -->
						<div class="contact-timing">
							<div class="module-heading">
								<h4 class="module-title">opening time</h4>
							</div>
							<!-- /.module-heading -->

							<div class="module-body outer-top-xs">
								<div class="table-responsive">
									<table class="table">
										<tbody>
											<tr>
												<td>Monday-Friday:</td>
												<td class="pull-right">08.00 To 18.00</td>
											</tr>
											<tr>
												<td>Saturday:</td>
												<td class="pull-right">09.00 To 20.00</td>
											</tr>
											<tr>
												<td>Sunday:</td>
												<td class="pull-right">10.00 To 20.00</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
								<p class="contact-number">Hot Line:(400)888 868 848</p>
							</div>
							<!-- /.module-body -->
						</div>
						<!-- /.contact-timing -->
						<!-- ============================================================= CONTACT TIMING : END ============================================================= -->
					</div>
					<!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-3">
						<!-- ============================================================= LATEST TWEET============================================================= -->
						<div class="latest-tweet">
							<div class="module-heading">
								<h4 class="module-title">latest tweet</h4>
							</div>
							<!-- /.module-heading -->

							<div class="module-body outer-top-xs">
								<div class="re-twitter">
									<div class="comment media">
										<div class="pull-left">
											<span class="icon fa-stack fa-lg"> <i
												class="fa fa-circle fa-stack-2x"></i> <i
												class="fa fa-twitter fa-stack-1x fa-inverse"></i>
											</span>
										</div>
										<div class="media-body">
											<a
												href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">@laurakalbag</a>
											As a result of your previous recommendation :) <span
												class="time"> 12 hours ago </span>
										</div>
									</div>

								</div>
								<div class="re-twitter">
									<div class="comment media">
										<div class="pull-left">
											<span class="icon fa-stack fa-lg"> <i
												class="fa fa-circle fa-stack-2x"></i> <i
												class="fa fa-twitter fa-stack-1x fa-inverse"></i>
											</span>
										</div>
										<div class="media-body">
											<a
												href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">@laurakalbag</a>
											As a result of your previous recommendation :) <span
												class="time"> 12 hours ago </span>
										</div>
									</div>
								</div>
							</div>
							<!-- /.module-body -->
						</div>
						<!-- /.contact-timing -->
						<!-- ============================================================= LATEST TWEET : END ============================================================= -->
					</div>
					<!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-3">
						<!-- ============================================================= INFORMATION============================================================= -->
						<div class="contact-information">
							<div class="module-heading">
								<h4 class="module-title">information</h4>
							</div>
							<!-- /.module-heading -->

							<div class="module-body outer-top-xs">
								<ul class="toggle-footer" style="">
									<li class="media">
										<div class="pull-left">
											<span class="icon fa-stack fa-lg"> <i
												class="fa fa-circle fa-stack-2x"></i> <i
												class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
											</span>
										</div>
										<div class="media-body">
											<p>868 Any Stress,Burala Casi,Picasa USA.</p>
										</div>
									</li>

									<li class="media">
										<div class="pull-left">
											<span class="icon fa-stack fa-lg"> <i
												class="fa fa-circle fa-stack-2x"></i> <i
												class="fa fa-mobile fa-stack-1x fa-inverse"></i>
											</span>
										</div>
										<div class="media-body">
											<p>
												(400) 0888 888 888<br>(400) 888 888 888
											</p>
										</div>
									</li>

									<li class="media">
										<div class="pull-left">
											<span class="icon fa-stack fa-lg"> <i
												class="fa fa-circle fa-stack-2x"></i> <i
												class="fa fa-envelope fa-stack-1x fa-inverse"></i>
											</span>
										</div>
										<div class="media-body">
											<span><a
												href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Contact
													@Unicase.com</a></span><br> <span><a
												href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Sale
													@Unicase.com</a></span>
										</div>
									</li>

								</ul>
							</div>
							<!-- /.module-body -->
						</div>
						<!-- /.contact-timing -->
						<!-- ============================================================= INFORMATION : END ============================================================= -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.links-social -->

		<div class="footer-bottom inner-bottom-sm">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="module-heading outer-bottom-xs">
							<h4 class="module-title">category</h4>
						</div>
						<!-- /.module-heading -->

						<div class="module-body">
							<ul class="list-unstyled">
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Order
										History</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Returns</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Libero
										Sed rhoncus</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Venenatis
										augue tellus</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">My
										Vouchers</a></li>
							</ul>
						</div>
						<!-- /.module-body -->
					</div>
					<!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="module-heading outer-bottom-xs">
							<h4 class="module-title">my account</h4>
						</div>
						<!-- /.module-heading -->

						<div class="module-body">
							<ul class="list-unstyled">
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">My
										Account</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Customer
										Service</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Privacy
										Policy</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Site
										Map</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Search
										Terms</a></li>
							</ul>
						</div>
						<!-- /.module-body -->
					</div>
					<!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="module-heading outer-bottom-xs">
							<h4 class="module-title">our services</h4>
						</div>
						<!-- /.module-heading -->

						<div class="module-body">
							<ul class="list-unstyled">
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Order
										History</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Returns</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Libero
										Sed rhoncus</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Venenatis
										augue tellus</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">My
										Vouchers</a></li>
							</ul>
						</div>
						<!-- /.module-body -->
					</div>
					<!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="module-heading outer-bottom-xs">
							<h4 class="module-title">help &amp; support</h4>
						</div>
						<!-- /.module-heading -->

						<div class="module-body">
							<ul class="list-unstyled">
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Knowledgebase</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Terms
										of Use</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Contact
										Support</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">Marketplace
										Blog</a></li>
								<li><a
									href="https://demo2.chethemes.com/html/unicase/index.php?page=homepage2#">About
										Unicase</a></li>
							</ul>
						</div>
						<!-- /.module-body -->
					</div>
				</div>
			</div>
		</div>

		<div class="copyright-bar">
			<div class="container">
				<div class="col-xs-12 col-sm-6 no-padding">
					<div class="copyright">
						Copyright © 2018 <a
							href="https://demo2.chethemes.com/html/unicase/index.php?page=home">NFC
							Store</a> - All rights Reserved
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 no-padding">
					<div class="clearfix payment-methods">
						<ul>
							<li><img src="/ui/static/assets/images/payments/1.png"
								alt=""></li>
							<li><img src="/ui/static/assets/images/payments/2.png"
								alt=""></li>
							<li><img src="/ui/static/assets/images/payments/3.png"
								alt=""></li>
							<li><img src="/ui/static/assets/images/payments/4.png"
								alt=""></li>
							<li><img src="/ui/static/assets/images/payments/5.png"
								alt=""></li>
						</ul>
					</div>
					<!-- /.payment-methods -->
				</div>
			</div>
		</div>
	</footer>
</body>
</html>